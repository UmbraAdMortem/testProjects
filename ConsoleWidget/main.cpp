#include "ConsoleWidget.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ConsoleWidget w;
    w.show();
    return a.exec();
}
