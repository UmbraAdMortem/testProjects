﻿#ifndef CONSOLEWIDGET_H
#define CONSOLEWIDGET_H

#include <QMainWindow>

#include <QProcess>
#include <QDir>

QT_BEGIN_NAMESPACE
namespace Ui { class ConsoleWidget; }
QT_END_NAMESPACE

class ConsoleWidget : public QMainWindow
{
    Q_OBJECT

public:
    explicit ConsoleWidget(QWidget *parent = nullptr);
    ~ConsoleWidget();

public slots:
    void changeDirectory(const QString& qstrDirectory2Change = QString(""));

protected:
    void changeEvent(QEvent *e);

private:
    Ui::ConsoleWidget *ui;
    QProcess* m_pProcess;
    QString m_qstrInputBuffer;
    QString m_qstrOutputBuffer;
    QStringList m_qstrlInputHistory;
    QDir m_qDir;

    void writeConsoleOutput(const QString& qstrOutput);

private slots:
    void onConsoleInputReturnPressed();

    void onProcessErrorOccured(QProcess::ProcessError error);
    void onProcessFinished(int iExitCode, QProcess::ExitStatus exitStatus);
    void onProcessReadStandardError();
    void onProcessReadStandardOutput();
    void onProcessStarted();
    void onProcessStateChanged(QProcess::ProcessState newState);
};

#endif // CONSOLEWIDGET_H
