﻿#include "ConsoleWidget.h"
#include "ui_ConsoleWidget.h"

#include <QDebug>

#define HISTORY_SIZE 100

ConsoleWidget::ConsoleWidget(QWidget *parent) :
    QMainWindow(parent, Qt::Widget),
    ui(new Ui::ConsoleWidget)
{
    ui->setupUi(this);
    connect(ui->leConsoleInput, &QLineEdit::returnPressed,
            this, &ConsoleWidget::onConsoleInputReturnPressed);

    m_pProcess = new QProcess(this);
    connect(m_pProcess, &QProcess::errorOccurred,
            this, &ConsoleWidget::onProcessErrorOccured);
    connect(m_pProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            this, &ConsoleWidget::onProcessFinished);
    connect(m_pProcess, &QProcess::readyReadStandardError,
            this, &ConsoleWidget::onProcessReadStandardError);
    connect(m_pProcess, &QProcess::readyReadStandardOutput,
            this, &ConsoleWidget::onProcessReadStandardOutput);
    connect(m_pProcess, &QProcess::started,
            this, &ConsoleWidget::onProcessStarted);
    connect(m_pProcess, &QProcess::stateChanged,
            this, &ConsoleWidget::onProcessStateChanged);

    changeDirectory();
}

ConsoleWidget::~ConsoleWidget()
{
    delete ui;
}

void ConsoleWidget::writeConsoleOutput(const QString &qstrOutput)
{
    ui->teConsoleOutput->moveCursor(QTextCursor::End);
    ui->teConsoleOutput->insertPlainText(qstrOutput + "\n");
    ui->teConsoleOutput->ensureCursorVisible();
}

void ConsoleWidget::changeDirectory(const QString& qstrDirectory2Change)
{
    if(m_qDir.cd(qstrDirectory2Change))
    {
        qDebug() << m_qDir.currentPath();
        qDebug() << m_qDir.absolutePath();
        ui->lCurrentDir->setText(m_qDir.absolutePath());
        m_pProcess->setWorkingDirectory(m_qDir.absolutePath());
        if(qstrDirectory2Change.isEmpty())
            writeConsoleOutput(m_qDir.absolutePath());
    }
    else
    {
        qWarning() << "Directory " + qstrDirectory2Change + " doesn't exist";
    }
}

void ConsoleWidget::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void ConsoleWidget::onConsoleInputReturnPressed()
{
    m_qstrInputBuffer = ui->leConsoleInput->text();
    ui->leConsoleInput->clear();

    m_qstrlInputHistory.prepend(m_qstrInputBuffer);
    while(m_qstrlInputHistory.size() > HISTORY_SIZE)
        m_qstrlInputHistory.removeLast();

    writeConsoleOutput(m_qstrInputBuffer);

    if(m_pProcess->state() == QProcess::NotRunning)
    {
        if(m_qstrInputBuffer.toLower().startsWith("cd ") ||
           m_qstrInputBuffer.toLower() == "cd")
        {
            changeDirectory(m_qstrInputBuffer.section(" ", 1, -1));
        }
        else
        {
            qDebug() << ">" + m_qstrInputBuffer.section(" ", 0, 0) + "<";
            qDebug() << ">" + m_qstrInputBuffer.section(" ", 1, -1) + "<";
            m_pProcess->setProgram(m_qstrInputBuffer.section(" ", 0, 0));
            m_pProcess->setArguments(m_qstrInputBuffer.split(" ").mid(1));
            m_pProcess->start();
        }
    }
    else
    {
        m_pProcess->write(m_qstrInputBuffer.toUtf8() + '\n');
    }
}

void ConsoleWidget::onProcessErrorOccured(QProcess::ProcessError error)
{
    qDebug() << error;
}

void ConsoleWidget::onProcessFinished(int iExitCode, QProcess::ExitStatus exitStatus)
{
    qDebug() << iExitCode << "\t" << exitStatus;
}

void ConsoleWidget::onProcessReadStandardError()
{
    m_qstrOutputBuffer = m_pProcess->readAllStandardError();
    qDebug() << m_qstrOutputBuffer;
    writeConsoleOutput(m_qstrOutputBuffer);
}

void ConsoleWidget::onProcessReadStandardOutput()
{
    m_qstrOutputBuffer = m_pProcess->readAllStandardOutput();
    qDebug() << m_qstrOutputBuffer;
    writeConsoleOutput(m_qstrOutputBuffer);
}

void ConsoleWidget::onProcessStarted()
{
    m_qstrOutputBuffer = m_pProcess->readAllStandardOutput();
    qDebug() << m_qstrOutputBuffer;
    writeConsoleOutput(m_qstrOutputBuffer);

}

void ConsoleWidget::onProcessStateChanged(QProcess::ProcessState newState)
{
    qDebug() << newState;
}

